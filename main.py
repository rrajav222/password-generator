from tkinter import *
from tkinter import messagebox
import random
import json

# ---------------------------- PASSWORD GENERATOR ------------------------------- #
# Password Generator Project
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
           'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']


def generatePassword():
    password_list = [random.choice(letters)
                     for _ in range(random.randint(8, 10))]
    password_list += [random.choice(symbols)
                      for _ in range(random.randint(2, 4))]
    password_list += [random.choice(numbers)
                      for _ in range(random.randint(2, 4))]

    random.shuffle(password_list)

    password = "".join(password_list)
    password_input.delete(0, END)
    password_input.insert(END, string=password)

# print(f"Your password is: {password}")

# ---------------------------- SAVE PASSWORD ------------------------------- #


def addPassword():
    add_button.flash()
    website_entry = website_input.get().lower()
    email_entry = email_input.get()
    password_entry = password_input.get()

    if len(website_entry.strip()) and len(email_entry.strip()) and len(password_entry.strip()):
        is_ok = messagebox.askokcancel(
            title=website_entry, message=f"These are the details entered\nEmail : {email_entry}\nPassword : {password_entry}\nDo you want to save?")
        if is_ok:
            new_data = {
                website_entry: {
                    "email": email_entry,
                    "password": password_entry
                }
            }
            try:
                with open("data.json", mode="r") as data_file:
                    try:
                        data = json.load(data_file)
                    except:
                        data = {}
                    finally:
                        data.update(new_data)
            except FileNotFoundError:
                with open("data.json", mode="w") as data_file:
                    json.dump(new_data, data_file, indent=4)
            else:
                with open("data.json", mode="w") as data_file:
                    json.dump(data, data_file, indent=4)

            website_input.delete(0, END)
            password_input.delete(0, END)

            messagebox.showinfo(
                title="Success", message=f"Email and password saved for {website_entry}")

    else:
        messagebox.showinfo(
            title="Error", message="Make sure all fields are filled")

# ---------------------------- SEARCH WEBSITE ------------------------------- #


def searchWebsite():
    website_entry = website_input.get().lower()
    if website_entry:
        try:
            with open("data.json", mode="r") as data_file:
                try:
                    data = json.load(data_file)
                except:
                    messagebox.showinfo(
                        title="Error", message="No data in file")
                else:
                    if website_entry in data:
                        messagebox.showinfo(
                            title="Success", message=f"Website : {website_entry}\nEmail : {data[website_entry]['email']}\nPassword : {data[website_entry]['password']}")
                    else:
                        messagebox.showinfo(
                            title="Error", message="Website not found")
        except FileNotFoundError:
            messagebox.showinfo(
                title="Error", message="No data file")
    else:
        messagebox.showinfo(
            title="Error", message="Enter a website name to search")

# ---------------------------- UI SETUP ------------------------------- #


window = Tk()
window.title("Password Generator")
window.minsize(width=500, height=300)
window.config(padx=50, pady=50)

canvas = Canvas(width=200, height=200)
lock_img = PhotoImage(file="logo.png")
canvas.create_image(110, 102, image=lock_img)
canvas.grid(column=1, row=0)

website_label = Label(text="Website:")
website_label.grid(column=0, row=1)

website_input = Entry(width=27)
website_input.grid(column=1, row=1, columnspan=1, pady=5)
website_input.focus()

search_button = Button(
    text="Search", command=searchWebsite, width=15)
search_button.grid(column=2, row=1)

email_label = Label(text="Email/Username:")
email_label.grid(column=0, row=2)

email_input = Entry(width=45)
email_input.insert(END, string="example@gmail.com")
email_input.grid(column=1, row=2, columnspan=2, pady=5)

password_label = Label(text="Password:")
password_label.grid(column=0, row=3, pady=5)

password_input = Entry(width=27)
password_input.grid(column=1, row=3)

generate_password_button = Button(
    text="Generate Password", command=generatePassword)
generate_password_button.grid(column=2, row=3)

add_button = Button(text="Add", width=36, command=addPassword)
add_button.grid(column=1, row=4, columnspan=2, pady=5)

window.mainloop()
